package suitebot.ai;

import org.junit.Test;
import suitebot.game.Direction;
import suitebot.game.GameState;
import suitebot.game.GameStateFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by l on 4/23/16.
 */
public class StickAiTest
{
    @Test
    public void shouldAvoidObstacles1() throws Exception
    {
        String gameStateAsString =
                " **\n" +
                " 1*\n" +
                " **\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new StickAi().makeMove(1, gameState).step1, is(Direction.LEFT));
    }
    
    @Test
    public void shouldAvoidObstacles2() throws Exception
    {
        String gameStateAsString =
                "***\n" +
                "*1 \n" +
                " **\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new StickAi().makeMove(1, gameState).step1, is(Direction.RIGHT));
    }
    
    @Test
    public void shouldAvoidObstacles3() throws Exception
    {
        String gameStateAsString =
                "* *\n" +
                "*1*\n" +
                " **\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new StickAi().makeMove(1, gameState).step1, is(Direction.UP));
    }
    
    @Test
    public void shouldAvoidObstacles4() throws Exception
    {
        String gameStateAsString =
                "***\n" +
                "*1*\n" +
                "  *\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new StickAi().makeMove(1, gameState).step1, is(Direction.DOWN));
    }

    @Test
    public void multiMoveTest() throws Exception
    {
        String gameStateAsString =
                " **\n" +
                " **\n" +
                " 1*\n" +
                " **\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.LEFT));
        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.LEFT));
        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.LEFT));
        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.LEFT));
        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.LEFT));
    }
    
    @Test
    public void outOfBoundsTest1() throws Exception
    {
        String gameStateAsString =
                " 1*\n" +
                " **\n" +
                " **\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.LEFT));
    }
    
    @Test
    public void outOfBoundsTest2() throws Exception
    {
        String gameStateAsString =
                " **\n" +
                " **\n" +
                "*1 \n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.RIGHT));
    }
    
    @Test
    public void outOfBoundsTest3() throws Exception
    {
        String gameStateAsString =
                "***\n" +
                "1 *\n" +
                "***\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.RIGHT));
    }
    
    @Test
    public void outOfBoundsTest4() throws Exception
    {
        String gameStateAsString =
                "***\n" +
                " *1\n" +
                "***\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.RIGHT));
    }
    
    @Test
    public void nearHeadsTest1()
    {
		String gameStateAsString =
                " ** \n" +
                " 2 1\n" +
                " ***\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.UP));
	}
	
	@Test
    public void nearHeadsTest2()
    {
		String gameStateAsString =
                " **2\n" +
                " ** \n" +
                "   1\n" +
                " ** \n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.LEFT));
	}
	
	@Test
    public void nearHeadsTest3()
    {
		String gameStateAsString =
                "** 1\n" +
                " ** \n" +
                "   2\n" +
                " ** \n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.LEFT));
	}
	
	@Test
    public void nearHeadsTest4()
    {
		String gameStateAsString =
                "**  \n" +
                "1 * \n" +
                "   2\n" +
                " ** \n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.RIGHT));
	}
	
	@Test
    public void nearHeadsTest5()
    {
		String gameStateAsString =
                "**  \n" +
                "2 1 \n" +
                "  3 \n" +
                " ** \n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.UP));
	}
	
	@Test
    public void lookAheadTest1()
    {
		String gameStateAsString =
                "** *\n" +
                "* 1 \n" +
                "**  \n" +
                " ** \n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.DOWN));
	}
	
	@Test
    public void lookAheadTest2()
    {
		String gameStateAsString =
                "** * \n" +
                "* 1  \n" +
                "** * \n" +
                " **  \n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.RIGHT));
	}
	
	@Test
    public void lookAheadTest3()
    {
		String gameStateAsString =
                "** * \n" +
                "* 1  \n" +
                "** * \n" +
                " * **\n" +
                " *** \n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        BotAi stickAi = new StickAi();

        assertThat(stickAi.makeMove(1, gameState).step1, is(Direction.RIGHT));
	}
}
