package suitebot.ai;

import org.junit.Test;
import suitebot.game.Direction;
import suitebot.game.GameState;
import suitebot.game.GameStateFactory;
import suitebot.game.Move;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SampleBotAiTest
{
	/*@Test
	public void shouldAvoidObstacles() throws Exception
	{
		String gameStateAsString =
				" **\n" +
				" 1*\n" +
				" **\n";

		GameState gameState = GameStateFactory.createFromString(gameStateAsString);

		assertThat(new SampleBotAi().makeMove(1, gameState).step1, is(Direction.LEFT));
	}

	@Test
	public void shouldCollectNearestTreasure() throws Exception
	{
		String gameStateAsString =
				"****\n" +
				"*1 !\n" +
				"*!  \n";

		GameState gameState = GameStateFactory.createFromString(gameStateAsString);

		assertThat(new SampleBotAi().makeMove(1, gameState).step1, equalTo(Direction.DOWN));
	}

	@Test
	public void shouldCollectTreasure_withDoubleMove() throws Exception
	{
		String gameStateAsString =
				"****\n" +
				"*1 !\n" +
				"*   \n";

		GameState gameState = GameStateFactory.createFromString(gameStateAsString);

		assertThat(new SampleBotAi().makeMove(1, gameState), equalTo(Move.RIGHT_RIGHT));
	}

	@Test
	public void shouldPreferTreasureToBattery_ifDistanceIsEqual() throws Exception
	{
		String gameStateAsString =
				"****\n" +
				"*1 !\n" +
				"* + \n";

		GameState gameState = GameStateFactory.createFromString(gameStateAsString);

		assertThat(new SampleBotAi().makeMove(1, gameState), equalTo(Move.RIGHT_RIGHT));
	}

	@Test
	public void shouldPreferBatteryToTreasure_ifCloser() throws Exception
	{
		String gameStateAsString =
				"****\n" +
				"*1 !\n" +
				"*+  \n";

		GameState gameState = GameStateFactory.createFromString(gameStateAsString);

		assertThat(new SampleBotAi().makeMove(1, gameState).step1, equalTo(Direction.DOWN));
	}*/
}
