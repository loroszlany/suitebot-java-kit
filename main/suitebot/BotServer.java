package suitebot;

import suitebot.ai.BotAi;
import suitebot.ai.StickAi;
import suitebot.server.SimpleServer;

public class BotServer
{
	public static final int DEFAULT_PORT = 9001;

	public static void main(String[] args)
	{
		BotAi botAi = new StickAi(); // replace with your own AI
		
		// commit test commit test commit test

		int port = determinePort(args);

		System.out.println("listening on port " + port);
		new SimpleServer(port, new BotRequestHandler(botAi)).run();
	}

	private static int determinePort(String[] args)
	{
		if (args.length == 1)
			return Integer.valueOf(args[0]);
		else
			return DEFAULT_PORT;
	}
}
