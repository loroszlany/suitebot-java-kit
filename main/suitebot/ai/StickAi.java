package suitebot.ai;

import com.google.common.collect.ImmutableList;
import suitebot.game.GameState;
import suitebot.game.Move;
import suitebot.game.Point;
import suitebot.ai.MutablePoint;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Sample AI. The AI has some serious flaws, which is intentional.
 */
public class StickAi implements BotAi
{
	private static final List<Move> SINGLE_MOVES = ImmutableList.of(
			Move.UP_, Move.LEFT_, Move.DOWN_, Move.RIGHT_);

	private static final List<Move> DOUBLE_MOVES = ImmutableList.of(
			Move.UP_UP, Move.UP_DOWN, Move.UP_LEFT, Move.UP_RIGHT,
			Move.DOWN_UP, Move.DOWN_DOWN, Move.DOWN_LEFT, Move.DOWN_RIGHT,
			Move.LEFT_UP, Move.LEFT_DOWN, Move.LEFT_LEFT, Move.LEFT_RIGHT,
			Move.RIGHT_UP, Move.RIGHT_DOWN, Move.RIGHT_LEFT, Move.RIGHT_RIGHT);

	private int botId;
	private GameState gameState;
	private static final int LIMIT = 8;

	private final Predicate<Move> isSafeMove = move -> !gameState.getObstacleLocations().contains(destination(move));
	private final Predicate<Move> isMoveFarFromHeads = move -> isFarFromHeads(destination(move));

	private final Stream<Move> safeMoveStream = SINGLE_MOVES.stream().filter(isSafeMove);

	/**
	 * if a safe move can be made (one that avoids any obstacles), do it;
	 * otherwise, go down.
	 */
	@Override
	public Move makeMove(int botId, GameState gameState)
	{
		this.botId = botId;
		this.gameState = gameState;

		if (isDead())
			return null;

		List<Move> safeMoveListLocal = new ArrayList<Move>();

		for (Move move : SINGLE_MOVES)
		{
			if(!gameState.getObstacleLocations().contains(destination(move)))
			{
				if (isFarFromHeads(destination(move)))
				{
					safeMoveListLocal.add(move);
				}
			}
		}

		Move selectedMove = Move.DOWN_;
		int selectedMoveValue = 0;

		for (Move move : safeMoveListLocal)
		{
			int evaluateResult = evaluate(destination(move));
			if(evaluateResult > selectedMoveValue)
			{
				selectedMove = move;
				selectedMoveValue = evaluateResult;

			}
		}

		return 	selectedMove;
	}

	private int evaluate(Point position)
	{
		Set<Point> blocked = new HashSet<Point>();
		blocked.addAll(gameState.getObstacleLocations());
		blocked.add(gameState.getBotLocation(botId));
		return evaluate(position, blocked, 0);

	}

	private int evaluate(Point position, Set<Point> blocked, int level)
	{
		Set<Point> blockedLocal = new HashSet<Point>(blocked);

		if(blockedLocal.contains(position) || level > LIMIT) return 1;
		blockedLocal.add(position);

		int max = 0;
		int result = 0;
		result = evaluate(detatchedDestination(position, Move.DOWN_), blockedLocal, level+1);
		if(result > max)
			max = result +1;
		result = evaluate(detatchedDestination(position, Move.LEFT_), blockedLocal, level+1);
		if(result > max)
			max = result +1;
		result = evaluate(detatchedDestination(position, Move.UP_), blockedLocal, level+1);
		if(result > max)
			max = result +1;
		result = evaluate(detatchedDestination(position, Move.RIGHT_), blockedLocal, level+1);
		if(result > max)
			max = result +1;

		return max;
	}

	private boolean isDead()
	{
		return !gameState.getLiveBotIds().contains(botId);
	}

	private boolean isFarFromHeads(Point destination)
	{
		Set<Integer> liveBotIds = gameState.getLiveBotIds();
		Set<Point> dangerPositions = new HashSet<Point>();
		for (Integer botId : liveBotIds)
		{
			if(botId != this.botId) {
				Point position = new Point(gameState.getBotLocation(botId).x, gameState.getBotLocation(botId).y);
				dangerPositions.add(new Point(position.x, position.y));
				dangerPositions.add(detatchedDestination(position, Move.DOWN_));
				dangerPositions.add(detatchedDestination(position, Move.UP_));
				dangerPositions.add(detatchedDestination(position, Move.LEFT_));
				dangerPositions.add(detatchedDestination(position, Move.RIGHT_));
			}
		}

		if(dangerPositions.contains(destination))
			return false;
		return true;
	}

	private Point destination(Move move)
	{
		Point botLocation = gameState.getBotLocation(botId);
		MutablePoint step1Destination = new MutablePoint(move.step1.from(botLocation).x, move.step1.from(botLocation).y);

		if(step1Destination.x >= gameState.getPlanWidth())
			step1Destination.x = 0;
		if(step1Destination.x < 0)
			step1Destination.x = gameState.getPlanWidth()-1;
		if(step1Destination.y >= gameState.getPlanHeight())
			step1Destination.y = 0;
		if(step1Destination.y < 0)
			step1Destination.y = gameState.getPlanHeight()-1;



		Point retval = new Point(step1Destination.x, step1Destination.y);

		if (move.step2 == null)
			return retval;
		else
			return move.step2.from(retval);
	}

	private Point detatchedDestination(Point botLocation, Move move)
	{
		MutablePoint step1Destination = new MutablePoint(move.step1.from(botLocation).x, move.step1.from(botLocation).y);

		if(step1Destination.x >= gameState.getPlanWidth())
			step1Destination.x = 0;
		if(step1Destination.x < 0)
			step1Destination.x = gameState.getPlanWidth()-1;
		if(step1Destination.y >= gameState.getPlanHeight())
			step1Destination.y = 0;
		if(step1Destination.y < 0)
			step1Destination.y = gameState.getPlanHeight()-1;


		Point retval = new Point(step1Destination.x, step1Destination.y);

		if (move.step2 == null)
			return retval;
		else
			return move.step2.from(retval);
	}

	@Override
	public String getName()
	{
		return "stick";
	}
}
